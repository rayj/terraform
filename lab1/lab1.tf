provider "aws" {
  region		= "us-east-1" 
  version 		= "~> 2.33"
}

resource "aws_instance" "terraformlab" {
  ami           	= "ami-2757f631"
  instance_type 	= "t2.micro"
}
