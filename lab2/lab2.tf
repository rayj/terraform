provider "aws" {
  region                = "us-east-1"
  version               = "~> 2.33"
}

resource "aws_instance" "tflab2" {
  ami           	= "ami-028be67c2aa2f1ce1"
  instance_type 	= "t2.micro"
  key_name 		= "terraform-key"
  security_groups 	= ["${aws_security_group.allow_rdp.name}"]
}

resource "aws_security_group" "allow_rdp" {
  name        		= "allow_rdp"
  description 		= "Allow ssh traffic"
  
  ingress {
  from_port 	  	= 3389
  to_port   	  	= 3389
  protocol 		= "tcp"
  cidr_blocks 		= ["0.0.0.0/0"]
  }
}
