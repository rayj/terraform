#!/bin/bash
cd /root/git/terraform
git config credential.helper store
git config --global credential.helper 'cache --timeout 28800'
git pull terraform master
git add --all
git diff --staged
git commit -m "Terraform Files and Configuration"
#git push newfiles master
git push
